const ip = 'https://api.ipify.org/?format=json';
const data = 'http://ip-api.com/json/';
const btn = document.querySelector('button');

async function findDataViaIP(ipLink, dataLink, element) {
    let responseIP = await fetch(ipLink);
    let {ip} = await responseIP.json();
    let responseData = await fetch(dataLink + ip);
    let {continent, country, region, city, district} = await responseData.json();
    element.insertAdjacentHTML('afterend', 
    `<p>Континент: ${continent}</p>
    <p>Країна: ${country}</p>
    <p>Регіон: ${region}</p>
    <p>Місто: ${city}</p>
    <p>Район: ${district}</p>`);
}

btn.addEventListener('click', () => findDataViaIP(ip, data, btn));
